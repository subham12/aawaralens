from crypt import methods
from flask import Flask, render_template, request, url_for, redirect, session
from pymongo import MongoClient
import bcrypt
import base64
import boto3
import datetime
import os
s3 = boto3.resource('s3')

#set app as a Flask instance 
app = Flask(__name__)
#encryption relies on secret keys so they could be run
#connoct to your Mongo DB database
app.secret_key="testing"

client = MongoClient("mongodb+srv://subhamsen100:Mypanda*99@cluster0.5bnan.mongodb.net/aawara?retryWrites=true&w=majority")

substring="data:image/png;base64"
#get the database name
db = client['aawara']
#get the particular collection that contains the data
records = db['register']

#assign URLs to have a particular route 
@app.route("/", methods=['post', 'get'])
def index():
    message = ''
    #if method post in index
    if "email" in session:
        return redirect(url_for("logged_in"))
    if request.method == "POST":
        bucket_name='aawara-signup-bucket'
        now = datetime.datetime.now()
        file_name="shot_{}.jpg".format(str(now))
        file_name=file_name.replace(" ","")
        user = request.form.get("fullname")
        email = request.form.get("email")
        password1 = request.form.get("password1")
        password2 = request.form.get("password2")
        phone=request.form.get('phone')
        category=request.form.get('category')
        file=request.form.get('profile_picture')
        print(file)
        if substring in file:
            base64img=file.replace("data:image/png;base64,",'')
        else:
            base64img=file.replace("data:image/jpeg;base64,",'')
        img_data=base64img.encode()
        print(img_data)
        
        s3url= "s3://"+bucket_name+file_name
        s3http = "https://%s.s3.%s.amazonaws.com/%s" % (bucket_name,"ap-south-1", file_name)
        #if found in database showcase that it's found 
        
        email_found = records.find_one({"email": email})
        phone_found=records.find_one({"phone":phone})
        if phone_found:
            message='This Phone Number is already registered'
        if email_found:
            message = 'This email already exists in database'
            return render_template('index.html', message=message)
        if password1 != password2:
            message = 'Passwords should match!'
            return render_template('index.html', message=message)
        else:
            #hash the password and encode it
            hashed = bcrypt.hashpw(password2.encode('utf-8'), bcrypt.gensalt())
            #assing them in a dictionary in key value pairs
            user_input = {'name': user, 'email': email, 'phone':phone,'password': hashed,'s3Url':s3url, 'category':category, 's3http':s3http}
            #insert it in the record collection
            records.insert_one(user_input)
            obj=s3.Bucket(bucket_name).Object(file_name)
            obj.put(Body=base64.decodebytes(img_data))
            print("Successful")
            print(obj)
            print(file_name)
            
            #find the new created account and its email
            user_data = records.find_one({"email": email})
            new_email = user_data['email']
            #if registered redirect to logged in as the registered user
            return redirect(url_for('login'))
    return render_template('index.html')



@app.route("/login", methods=["POST", "GET"])
def login():
    cat=request.form.get("email")
    print(cat)
    message = 'Please login to your account'
    if "email" in session:
        query={'category':cat}
        doc=records.find(query)
        for x in doc:
            z=x.get('category')
        if z=="user":
            return redirect(url_for("logged_in"))
        else:
            return redirect(url_for("admin_logged"))

    if request.method == "POST":
        email = request.form.get("email")
        password = request.form.get("password")

        #check if email exists in database
        email_found = records.find_one({"email": email})
        if email_found:
            email_val = email_found['email']
            passwordcheck = email_found['password']
            #encode the password and check if it matches
            if bcrypt.checkpw(password.encode('utf-8'), passwordcheck):
                session["email"] = email_val
                query={'email':cat}

                doc=records.find_one(query)
                print(type(doc))
                doc=doc['category']
                z=doc
                
                print(z)
                if z=="user":
                    return redirect(url_for("logged_in"))
                else:
                    return redirect(url_for("admin_logged"))
            else:
                if "email" in session:
                    query={'category':cat}
                    doc=records.find(query)
                    for x in doc:
                        z=x.get('category')
                    print(z)
                    if z=="user":
                        return redirect(url_for("logged_in"))
                    else:
                        return redirect(url_for("admin_logged"))
                message = 'Wrong password'
                return render_template('login.html', message=message)
        else:
            message = 'Email not found'
            return render_template('login.html', message=message)
    return render_template('login.html', message=message)

@app.route('/logged_in',methods=['POST','GET'])
def logged_in():
    if "email" in session:
        
        email = session["email"]
        searchRes=db['searchResults']
        myquery={'email':email}
        
        
        if request.method=='POST':
            doc=None
            email = session["email"]
            doc=searchRes.find(myquery)
            
            if doc == None:
                print('hello')
                return render_template('logged_in.html', email=email)
            else:
                for items in doc:
                    print(items)
                users3=items['s3http']
                print(users3)

                return render_template('logged_in.html', email=email,users3=users3)
        else:
            return render_template('logged_in.html', email=email)
        
    else:
        return redirect(url_for("login"))

@app.route("/logout", methods=["POST", "GET"])
def logout():
    if "email" in session:
        session.pop("email", None)
        return render_template("signout.html")
    else:
        return render_template('index.html')


@app.route("/admin_logged",methods=["POST","GET"])
def admin_logged():
    if "email" in session:
        if request.method=='POST':
            bucket_name="aawara-image-upload2"
            email = session["email"]
            upload=request.form.get('uploaded_picture')
            now = datetime.datetime.now()
            file_name="shot_{}.jpg".format(str(now))
            file_name=file_name.replace(" ","")
            print(file_name)
            if substring in upload:
                base64img=upload.replace("data:image/png;base64,",'')
            else:
                base64img=upload.replace("data:image/jpeg;base64,",'')
            img_data2=base64img.encode()
            print(img_data2)
            obj=s3.Bucket(bucket_name).Object(file_name)
            obj.put(Body=base64.decodebytes(img_data2),ContentType='image/jpeg', ACL='public-read')

        return render_template('admin_login.html')
    
    else:
        return redirect(url_for("login"))




if __name__ == "__main__":
  app.run(debug=True)
